package net.sf.okapi.filters.openxml;

import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.LocaleId;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.FileLocation;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TestPowerpointDocument {
	private XMLFactoriesForTest factories = new XMLFactoriesForTest();
	private FileLocation root;

	@Before
	public void setUp() {
		root = FileLocation.fromClass(getClass());
	}

	@Test
	public void testSlides() throws Exception {
		Document.General doc = generalDocument("/slideLayouts.pptx", new ConditionalParameters());
		doc.open();
		List<String> expected = new ArrayList<>();
		expected.add("ppt/slides/slide1.xml");
		expected.add("ppt/slides/slide2.xml");
		assertEquals(expected, ((PowerpointDocument) doc.categorisedDocument()).findSlides());
	}

	@Test
	public void testSlideLayouts() throws Exception {
		Document.General doc = generalDocument("/slideLayouts.pptx", new ConditionalParameters());
		doc.open();
		List<String> expected = new ArrayList<>();
		expected.add("ppt/slideLayouts/slideLayout1.xml");
		expected.add("ppt/slideLayouts/slideLayout2.xml");
		assertEquals(expected, ((PowerpointDocument) doc.categorisedDocument()).findSlideLayouts(((PowerpointDocument) doc.categorisedDocument()).findSlides()));
	}

	private Document.General generalDocument(String resource, ConditionalParameters params) throws Exception {
		return new Document.General(
			params,
			factories.getInputFactory(),
			factories.getOutputFactory(),
			factories.getEventFactory(),
			"sd",
			root.in(resource).asUri(),
			LocaleId.ENGLISH,
			OpenXMLFilter.ENCODING.name(),
			null,
			null,
			null
		);
	}
}
