/*
 * =============================================================================
 * Copyright (C) 2010-2020 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.ListIterator;

/**
 * Provides a markup clarifier.
 */
class MarkupClarifier {
    private final MarkupComponentClarifier.SheetViewClarifier sheetViewClarifier;
    private final MarkupComponentClarifier.AlignmentClarifier alignmentClarifier;
    private final MarkupComponentClarifier.PresentationClarifier presentationClarifier;
    private final BlockPropertiesClarifier.TablePropertiesClarifier tablePropertiesClarifier;
    private final BlockPropertiesClarifier.TextBodyPropertiesClarifier textBodyPropertiesClarifier;
    private final BlockPropertiesClarifier.ParagraphPropertiesClarifier paragraphPropertiesClarifier;
    private final StylesClarifier wordStylesClarifier;

    /**
     * Constructs the markup clarifier.
     * @param sheetViewClarifier The sheet view clarifier
     * @param alignmentClarifier The alignment clarifier
     * @param presentationClarifier The presentation clarifier
     * @param tablePropertiesClarifier The table properties clarifier
     * @param textBodyPropertiesClarifier The text body properties clarifier
     * @param paragraphPropertiesClarifier The paragraph properties clarifier
     * @param wordStylesClarifier The word styles clarifier
     */
    MarkupClarifier(
        final MarkupComponentClarifier.SheetViewClarifier sheetViewClarifier,
        final MarkupComponentClarifier.AlignmentClarifier alignmentClarifier,
        final MarkupComponentClarifier.PresentationClarifier presentationClarifier,
        final BlockPropertiesClarifier.TablePropertiesClarifier tablePropertiesClarifier,
        final BlockPropertiesClarifier.TextBodyPropertiesClarifier textBodyPropertiesClarifier,
        final BlockPropertiesClarifier.ParagraphPropertiesClarifier paragraphPropertiesClarifier,
        final StylesClarifier wordStylesClarifier
    ) {
        this.sheetViewClarifier = sheetViewClarifier;
        this.alignmentClarifier = alignmentClarifier;
        this.presentationClarifier = presentationClarifier;
        this.tablePropertiesClarifier = tablePropertiesClarifier;
        this.textBodyPropertiesClarifier = textBodyPropertiesClarifier;
        this.paragraphPropertiesClarifier = paragraphPropertiesClarifier;
        this.wordStylesClarifier = wordStylesClarifier;
    }

    /**
     * Clarifies markup.
     */
    void clarify(Markup markup) {
        ListIterator<MarkupComponent> iterator = markup.components().listIterator();

        while (iterator.hasNext()) {
            MarkupComponent component = iterator.next();

            if (MarkupComponent.isSheetViewStart(component)) {
                this.sheetViewClarifier.clarify(component);
            } else if (MarkupComponent.isAlignmentEmptyElement(component)) {
                this.alignmentClarifier.clarify(component);
            } else if (MarkupComponent.isPresentationStart(component)) {
                this.presentationClarifier.clarify(component);
            } else if (MarkupComponent.isTableStart(component)) {
                this.tablePropertiesClarifier.clarifyWith(iterator);
            } else if (MarkupComponent.isTextBodyStart(component)) {
                this.textBodyPropertiesClarifier.clarifyWith(iterator);
            } else if (MarkupComponent.isParagraphStart(component)) {
                this.paragraphPropertiesClarifier.clarifyWith(iterator);
            } else if (MarkupComponent.isWordStylesStart(component)) {
                this.wordStylesClarifier.clarifyWith(iterator);
            }
        }
    }
}
